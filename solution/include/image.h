#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>

//струтктура для хранения изображения
struct image {
    uint64_t width, height;
    struct pixel* data;
};

//структура для хранения одного пикселя
struct pixel { uint8_t b, g, r; };


