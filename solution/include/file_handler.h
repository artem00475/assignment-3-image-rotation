#include <stdio.h>

//коды состояния для открытия файла
enum open_status {
    OPEN_OK = 0,
    OPEN_FAILED
};

//функция для открытия файла (от имени файла, указателя на дискриптор файла, доступ к файлу)
enum open_status open_file(char* filename, FILE** file, size_t type);

//коды состояния для закрытия файла
enum close_status {
    CLOSE_OK = OPEN_FAILED+1,
    CLOSE_FAILED
};

//функция для закрытия файла (от дискриптора файла)
enum close_status close_file(FILE* file);



