#include "../include/image.h"
#include <stdint.h>
#include <stdio.h>
//значения для заголовка bmp-файла
enum bmp_header_values {
BMP_SIGNATURE =0x4D42,
BIT_MAP_INFO_HEADER =40,
PLANES_NUMBER =1,
BITS_PER_PIXEL =24,
COMPRESSION_TYPE =0,
NONE =0
};

//структура заголовка bmp-файла
struct __attribute__((packed)) bmp_header;

//коды состояния при чтении bmp-файла
enum read_status  {
    READ_OK = 4,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEMORY_ERROR,
    READ_PIXELS_ERROR,
    READ_INVALID_BIT_COUNT

};

//коды состояния при записи в bmp-файл
enum  write_status  {
    WRITE_OK = READ_INVALID_BIT_COUNT+1,
    WRITE_ERROR
};
//функция для получения изображения из bmp-файла
enum read_status from_bmp( FILE* in, struct image* img);
//функция для записи изображения в bmp-файл
enum write_status to_bmp( FILE* out, struct image const* img);
