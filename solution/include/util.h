//
// Created by Артём on 11.01.2023.
//

#ifndef LAB3_UTIL_H
#define LAB3_UTIL_H
#include "../include/bmp_file_controller.h"
#include "../include/file_handler.h"

enum error_status {
    ROTATE_FAIL = WRITE_ERROR+1,
    ARGUMENTS_ERROR
};

void print_error(int enum_status);

#endif //LAB3_UTIL_H

