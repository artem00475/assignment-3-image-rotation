#include "../include/file_handler.h"

//функция для открытия файла (от имени файла, указателя на дискриптор файла, доступ к файлу)
enum open_status open_file(char* filename, FILE** file, size_t type) {
    if (type == 0) {
        if ((*file = fopen(filename, "r")) == NULL) return OPEN_FAILED; //если файл не удалось открыть, то ошибка
    } else if (type == 1) {
        if ((*file = fopen(filename, "w")) == NULL) return OPEN_FAILED;
    }
    else return OPEN_FAILED;
    return OPEN_OK;
}

//функция для закрытия файла (от дискриптора файла)
enum close_status close_file(FILE* file) {
    if (fclose(file) == 0) return CLOSE_OK;
    return CLOSE_FAILED;
}
