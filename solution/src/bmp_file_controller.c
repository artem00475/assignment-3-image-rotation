#include "../include/bmp_file_controller.h"
#include <inttypes.h>

//структура заголовка bmp-файла
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

struct image create_image (const uint64_t width,const uint64_t height);

//функция для получения изображения из bmp-файла
enum read_status from_bmp( FILE* in, struct image* img) {
    struct bmp_header bmp_header;
    if (fread(&bmp_header,sizeof(bmp_header),1,in) == 0) { //считываем заголовок в struct bmp_header
        return READ_INVALID_HEADER;
    }
    if (bmp_header.bfType != BMP_SIGNATURE) { //проверка типа файла
        return READ_INVALID_SIGNATURE;
    }
    if (bmp_header.biBitCount != BITS_PER_PIXEL) {
        return READ_INVALID_BIT_COUNT;
    }
    *img = create_image(bmp_header.biWidth, bmp_header.biHeight);  //создание struct image
    if (img->data == NULL) {
        return READ_MEMORY_ERROR;
    }
    int64_t padding = 4 - (bmp_header.biWidth*3)%4; //вычисляем выравнивание
    for (size_t i = 0; i <bmp_header.biHeight; i++) { //считываем пиксели из bmp-файла
        if (fread((img->data)+i*bmp_header.biWidth,sizeof(struct pixel) * bmp_header.biWidth,1,in)==0) { //считываем по строкам пиксели
            return READ_PIXELS_ERROR;
        }
        if (fseek(in,padding,SEEK_CUR) !=0) { //выполняем смещение текущего положения в файле на выравнивание
            return READ_PIXELS_ERROR;
        }
    }
    return READ_OK;
}

//функция для записи изображения в bmp-файл
enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint32_t image_size = (img->height) * (sizeof(struct pixel) * (img->width) + 4 - (img->width*3)%4); //считаем размер изображения

    struct bmp_header bmp_header = (struct bmp_header){  //создаем заголовок bmp-файла
            .bfType = BMP_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BIT_MAP_INFO_HEADER,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES_NUMBER,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = COMPRESSION_TYPE,
            .biSizeImage = image_size,
            .biXPelsPerMeter = NONE,
            .biYPelsPerMeter = NONE,
            .biClrUsed = NONE,
            .biClrImportant = NONE
    };

    if (fwrite(&bmp_header, sizeof(struct bmp_header),1,out) == 0) return WRITE_ERROR; //записываем заголовок в файл

    uint32_t padding = 4 - (img->width*3)%4; //считаем выравнивание
    uint32_t empty_byte = 0; //байты для выравнивания

    for (size_t i = 0; i < img->height ; ++i) {
        if (fwrite(img->data + i*img->width, sizeof(struct pixel)*img->width,1,out)==0) return WRITE_ERROR; //записываем пиксели в файл
        if (fwrite(&empty_byte, padding,1,out)==0 && padding!=0) return WRITE_ERROR; //выполняем выравнивание
    }

    return WRITE_OK;
}
