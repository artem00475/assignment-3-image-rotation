#include "../include/image.h"

//функция для создания struct image (от размеров изображения)
struct image create_image(const uint64_t width,const uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * (width * height));
    return (struct image){.width = width, .height = height, .data = data};
}

//функция для очистки памяти струткуры
void free_struct_image(struct image image){
    free(image.data);
    image.height = 0;
    image.width = 0;
}

