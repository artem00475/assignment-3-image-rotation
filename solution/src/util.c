//
// Created by Артём on 11.01.2023.
//

#include "util.h"

void print_error(int enum_status) {
    switch (enum_status) {
        case READ_INVALID_HEADER:
            fprintf(stderr,"%s\n","Invalid file header");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr,"%s\n","Invalid file signature");
            break;
        case READ_INVALID_BIT_COUNT:
            fprintf(stderr,"%s\n","Invalid bit count");
            break;
        case READ_MEMORY_ERROR:
            fprintf(stderr,"%s\n","Memory error");
            break;
        case READ_PIXELS_ERROR:
            fprintf(stderr,"%s\n","Pixels read fail");
            break;
        case WRITE_ERROR:
            fprintf(stderr,"%s\n","Write to file fail");
            break;
        case OPEN_FAILED:
            fprintf(stderr,"%s\n","File not opened");
            break;
        case CLOSE_FAILED:
            fprintf(stderr,"%s\n","File not closed");
            break;
        case ROTATE_FAIL:
            fprintf(stderr,"%s\n","Rotate fail");
            break;
        case ARGUMENTS_ERROR:
            fprintf(stderr,"%s\n","Wrong number of arguments");
            break;
    }
}
