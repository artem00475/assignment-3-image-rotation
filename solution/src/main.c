#include "../include/transform_image.h"
#include "../include/util.h"
#include <stdio.h>

void free_struct_image(struct image image);

//названия файлов для чтения и записи передаются через аргументы командной строки в виде: ./image-transformer <source-image> <transformed-image>
int main(int argc, char** argv) {


    //проверяем количество аргументов
    if (argc !=3) {
        print_error(ARGUMENTS_ERROR);
        return 0;
    }
    //открываем файл для чтения
    FILE* file;
    if (open_file(argv[1], &file, 0) == OPEN_FAILED) {
        print_error(OPEN_FAILED);
        return 0;
    }
    //считываем bmp-файл в struct image
    struct image image;
    int status = from_bmp(file,&image);
    if(status != READ_OK) {
        print_error(status);
        return 0;
    }

    //закрываем текущий файл
    if (close_file(file) == CLOSE_FAILED) {
        print_error(CLOSE_FAILED);
        free_struct_image(image);
        return 0;
    }

    //создаем копию struct image с перевернутым в ней изображением
    struct image rotated_image = rotate(image);
    if (rotated_image.data == NULL) {
        free_struct_image(image);
        free_struct_image(rotated_image);
        print_error(ROTATE_FAIL);
        return 0;
    }
    free_struct_image(image);

    //открываем файл для записи
    if (open_file(argv[2], &file, 1) == OPEN_FAILED) {
        free_struct_image(rotated_image);
        print_error(OPEN_FAILED);
        return 0;
    }

    //записываем struct image в bmp-файл
    if (to_bmp(file,&rotated_image) == WRITE_ERROR) {
        free_struct_image(rotated_image);
        print_error(WRITE_ERROR);
        return 0;
    }

    //закрываем текущий файл
    if (close_file(file) == CLOSE_FAILED) {
        print_error(CLOSE_FAILED);
    }
    free_struct_image(rotated_image);
    printf("%s\n","Success");
    return 0;
}
