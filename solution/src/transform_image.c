#include "../include/transform_image.h"
#include "../include/image.h"
#include <stddef.h>

struct image create_image (const uint64_t width,const uint64_t height);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source ) {
    struct image image = create_image(source.height,source.width); //создаем копии изображения
    if (image.data == NULL) return image; //если не выделена память, то ошибка
    for (size_t i = 0;i<source.width;i++) {  //выполняем поворот изображения на 90 градусов
        for (size_t j = 0;j<source.height;j++) {
            image.data[(i*source.height)+j] = source.data[i+(source.height - 1 -j)*source.width];
        }
    }
    return image;
}
